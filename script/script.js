
let liEls = document.querySelectorAll('.pro-container .pro-image');
let index = 0;
window.show = function(increase) {
  index = index + increase;
  index = Math.min(Math.max(index,0), liEls.length-1);

  liEls[index].scrollIntoView({behavior: 'smooth'});
}

 // navegation icon responsible mobile tablets

 menu = document.querySelector('.menu');

menu.addEventListener("click", () => {
  document.querySelector(".navbar").classList.toggle("change");
});